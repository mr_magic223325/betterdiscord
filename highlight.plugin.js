/**
  * @name highlight
  * @author mr_magic223325
  * @description Highlight Text
  * @version 0.0.1
*/

module.exports = class Highlight {
  constructor(meta) {};
  start() {};

  observer() {

    const doc = document;
    const alert = "#c62809";
    const query = "DodgerBlue";
    const todo  = "DarkOrange";
    const highlight = "#98bd5c";
    const comment = "DarkGray";
    const white = "#DCDDDE";

    const msgs = document.querySelectorAll(".markup-eYLPri");
    const msgField = document.querySelector(".editor-H2NA06");

    msgs.forEach(msg => {
      switch(msg.innerText.split('')[0]) {
        case '!':
          msg.style.color = `${alert}`;
          msg.style.fontWeight = "bold";
          break;

        case '?':
          msg.style.color = `${query}`;
          break;

        case '*':
          msg.style.color = `${highlight}`;
          msg.style.fontWeight = "bold";
          break;

        default:
          msgField.style.color = `${white}`;
          msgField.style.fontWeight = "normal";
          break;
      };
      if (msg.innerText.toLowerCase().startsWith("todo")) {
        msg.style.color = `${todo}`;
        msg.style.fontWeight = "bold";
      }
      if (msg.innerText.startsWith("///")) {
        msg.style.color = `${comment}`;
        msg.style.fontStyle = "italic";
      }
    });
  }
  stop() {}
};
