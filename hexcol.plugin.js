/**
  * @name hexcol
  * @author mr_magic223325
  * @description Color Hexadecimal Color Codes
  * @version 0.0.1
*/

'use strict'

module.exports = class HexCol {
  constructor(meta) {};
  start() {};

  async observer() {
    const doc = document;
    const msgs = doc.querySelectorAll(".markup-eYLPri");

    msgs.forEach(msg => {
      if (msg.classList.contains("editor-H2NA06")) {
        return;
      }
      if (!msg.innerHTML.includes('<span id="HEXCOL"')) {
        if (isHex().test(msg.innerText)) {
          let match = isHex().exec(msg.innerText);
          let hex = msg.innerText.split('')[match.index];
          for (let i = 1; i < 7; i++) {
            if (msg.innerText.split('')[match.index + i] == undefined) {
              break;
            }
            hex += msg.innerText.split('')[match.index + i];
          }
          if (/^#([0-9A-F]{3}){1,2}$/i.test(hex)) {
          const final = msg.innerHTML.replace(hex, `<span id="HEXCOL" style="color: ${hex};">${hex}</span>`);
          msg.innerHTML = final;
          }
        };
        return;
      };
    });
  };
  stop() {
    let elements = doc.querySelectorAll("#HEXCOL");
    elements.forEach(element => {
      element.remove();
    });
  };
};

function isHex(opts) {
    opts = opts && typeof opts === 'object' ? opts : {};

    return opts.strict
      ? /^#([a-f0-9]{3,4}|[a-f0-9]{4}(?:[a-f0-9]{2}){1,2})\b$/i
      : /#([a-f0-9]{3}|[a-f0-9]{4}(?:[a-f0-9]{2}){0,2})\b/gi;
}

function sleep(time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}
